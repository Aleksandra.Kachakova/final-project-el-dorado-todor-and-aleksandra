/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 79.43444730077121, "KoPercent": 20.565552699228792};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.6735218508997429, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.85, 500, 1500, "Go to Latest Posts page"], "isController": false}, {"data": [0.7333333333333333, 500, 1500, "Go to Login page"], "isController": false}, {"data": [0.9, 500, 1500, "Go to Users page"], "isController": false}, {"data": [0.5, 500, 1500, "Log out"], "isController": false}, {"data": [0.8, 500, 1500, "Go to a specific post"], "isController": false}, {"data": [1.0, 500, 1500, "Go to About Us page"], "isController": false}, {"data": [0.5, 500, 1500, "Log out-1"], "isController": false}, {"data": [0.1896551724137931, 500, 1500, "Go to Home page"], "isController": false}, {"data": [1.0, 500, 1500, "Log out-0"], "isController": false}, {"data": [0.16666666666666666, 500, 1500, "Log into user\'s account"], "isController": false}, {"data": [0.9333333333333333, 500, 1500, "Go to a specific user"], "isController": false}, {"data": [0.43333333333333335, 500, 1500, "Log into user\'s account-0"], "isController": false}, {"data": [0.7333333333333333, 500, 1500, "Log into user\'s account-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 389, 80, 20.565552699228792, 496.21336760925425, 131, 2082, 252.0, 1454.0, 1706.5, 1877.1000000000013, 70.6630336058129, 481.255321866485, 15.82829529972752], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["Go to Latest Posts page", 30, 4, 13.333333333333334, 275.93333333333345, 159, 510, 247.5, 426.6, 499.55, 510.0, 18.315018315018317, 320.81091651404154, 3.4698374542124544], "isController": false}, {"data": ["Go to Login page", 30, 7, 23.333333333333332, 256.1666666666667, 170, 1769, 203.5, 231.9, 924.1999999999989, 1769.0, 16.75041876046901, 85.69703988693468, 2.3064541457286434], "isController": false}, {"data": ["Go to Users page", 30, 3, 10.0, 217.29999999999998, 161, 389, 197.5, 295.5, 348.84999999999997, 389.0, 18.58736059479554, 128.67390799256503, 3.5214335501858733], "isController": false}, {"data": ["Log out", 30, 15, 50.0, 332.33333333333337, 283, 449, 329.0, 382.20000000000005, 418.19999999999993, 449.0, 20.408163265306122, 101.1977838010204, 7.892219387755103], "isController": false}, {"data": ["Go to a specific post", 30, 6, 20.0, 278.46666666666664, 175, 450, 266.5, 430.40000000000015, 448.35, 450.0, 17.857142857142858, 163.81370907738096, 3.452845982142857], "isController": false}, {"data": ["Go to About Us page", 30, 0, 0.0, 150.43333333333334, 137, 185, 149.0, 159.70000000000002, 171.79999999999998, 185.0, 22.813688212927758, 178.57503564638785, 4.322124524714829], "isController": false}, {"data": ["Log out-1", 30, 15, 50.0, 188.56666666666672, 146, 305, 182.0, 235.50000000000003, 275.84999999999997, 305.0, 22.50562640660165, 101.86213545573894, 4.417608308327082], "isController": false}, {"data": ["Go to Home page", 29, 13, 44.827586206896555, 1257.7931034482758, 744, 1677, 1365.0, 1611.0, 1677.0, 1677.0, 17.088980553918677, 102.6771738730112, 2.2696302298173245], "isController": false}, {"data": ["Log out-0", 30, 0, 0.0, 143.4, 131, 158, 144.0, 151.0, 154.15, 158.0, 22.865853658536587, 9.892161299542682, 4.354337366615853], "isController": false}, {"data": ["Log into user\'s account", 30, 8, 26.666666666666668, 1549.3666666666666, 724, 2082, 1706.5, 1917.9, 2046.25, 2082.0, 12.875536480686696, 110.55861051502146, 5.863985380901287], "isController": false}, {"data": ["Go to a specific user", 30, 2, 6.666666666666667, 277.83333333333337, 186, 385, 281.5, 361.8, 383.35, 385.0, 20.491803278688522, 181.4605212602459, 3.9422707479508197], "isController": false}, {"data": ["Log into user\'s account-0", 30, 0, 0.0, 1242.0666666666666, 280, 1855, 1360.5, 1723.4000000000003, 1797.25, 1855.0, 14.85148514851485, 7.460067295792079, 4.01744275990099], "isController": false}, {"data": ["Log into user\'s account-1", 30, 7, 23.333333333333332, 306.50000000000006, 177, 680, 290.5, 486.80000000000007, 582.0999999999999, 680.0, 15.045135406218655, 121.63100630015045, 2.7822725990471415], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: text expected to contain \\\/Log out\\\/", 1, 1.25, 0.2570694087403599], "isController": false}, {"data": ["500", 79, 98.75, 20.308483290488432], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 389, 80, "500", 79, "Test failed: text expected to contain \\\/Log out\\\/", 1, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["Go to Latest Posts page", 30, 4, "500", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["Go to Login page", 30, 7, "500", 7, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["Go to Users page", 30, 3, "500", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["Log out", 30, 15, "500", 15, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["Go to a specific post", 30, 6, "500", 6, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["Log out-1", 30, 15, "500", 15, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["Go to Home page", 29, 13, "500", 13, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["Log into user\'s account", 30, 8, "500", 7, "Test failed: text expected to contain \\\/Log out\\\/", 1, null, null, null, null, null, null], "isController": false}, {"data": ["Go to a specific user", 30, 2, "500", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["Log into user\'s account-1", 30, 7, "500", 7, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
