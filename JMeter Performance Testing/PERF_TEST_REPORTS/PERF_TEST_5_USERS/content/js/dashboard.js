/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.921875, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Go to Latest Posts page"], "isController": false}, {"data": [1.0, 500, 1500, "Go to Login page"], "isController": false}, {"data": [1.0, 500, 1500, "Go to Users page"], "isController": false}, {"data": [1.0, 500, 1500, "Log out"], "isController": false}, {"data": [1.0, 500, 1500, "Go to a specific post"], "isController": false}, {"data": [1.0, 500, 1500, "Go to About Us page"], "isController": false}, {"data": [1.0, 500, 1500, "Log out-1"], "isController": false}, {"data": [0.375, 500, 1500, "Go to Home page"], "isController": false}, {"data": [1.0, 500, 1500, "Log out-0"], "isController": false}, {"data": [0.5, 500, 1500, "Log into user\'s account"], "isController": false}, {"data": [1.0, 500, 1500, "Go to a specific user"], "isController": false}, {"data": [1.0, 500, 1500, "Log into user\'s account-0"], "isController": false}, {"data": [1.0, 500, 1500, "Log into user\'s account-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 64, 0, 0.0, 296.57812499999994, 141, 1521, 186.5, 529.0, 1179.75, 1521.0, 18.32235900372173, 142.95974762024048, 4.124879222731177], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["Go to Latest Posts page", 5, 0, 0.0, 183.0, 177, 187, 183.0, 187.0, 187.0, 187.0, 24.752475247524753, 488.64383508663366, 4.689433787128713], "isController": false}, {"data": ["Go to Login page", 5, 0, 0.0, 190.2, 178, 204, 188.0, 204.0, 204.0, 204.0, 24.509803921568626, 138.0112591911765, 3.3748851102941178], "isController": false}, {"data": ["Go to Users page", 5, 0, 0.0, 172.4, 159, 180, 173.0, 180.0, 180.0, 180.0, 24.390243902439025, 179.66368140243904, 4.6208079268292686], "isController": false}, {"data": ["Log out", 5, 0, 0.0, 314.0, 310, 317, 314.0, 317.0, 317.0, 317.0, 14.577259475218659, 88.38887117346938, 5.637299562682215], "isController": false}, {"data": ["Go to a specific post", 5, 0, 0.0, 194.4, 179, 230, 188.0, 230.0, 230.0, 230.0, 21.367521367521366, 229.84692174145297, 4.131610576923077], "isController": false}, {"data": ["Go to About Us page", 5, 0, 0.0, 145.8, 143, 148, 146.0, 148.0, 148.0, 148.0, 28.089887640449437, 221.2627282303371, 5.321716994382022], "isController": false}, {"data": ["Log out-1", 5, 0, 0.0, 170.4, 167, 172, 171.0, 172.0, 172.0, 172.0, 24.875621890547265, 140.07112873134326, 4.8828125], "isController": false}, {"data": ["Go to Home page", 4, 0, 0.0, 1297.25, 1032, 1521, 1318.0, 1521.0, 1521.0, 1521.0, 2.619515389652914, 21.582862639161757, 0.3479043876882777], "isController": false}, {"data": ["Log out-0", 5, 0, 0.0, 142.8, 141, 144, 143.0, 144.0, 144.0, 144.0, 28.90173410404624, 12.503386921965319, 5.503748193641619], "isController": false}, {"data": ["Log into user\'s account", 5, 0, 0.0, 527.0, 519, 532, 527.0, 532.0, 532.0, 532.0, 9.00900900900901, 92.16638513513513, 4.099802927927928], "isController": false}, {"data": ["Go to a specific user", 5, 0, 0.0, 194.2, 181, 209, 192.0, 209.0, 209.0, 209.0, 22.42152466367713, 208.97561659192826, 4.313515975336323], "isController": false}, {"data": ["Log into user\'s account-0", 5, 0, 0.0, 336.6, 325, 347, 338.0, 347.0, 347.0, 347.0, 13.66120218579235, 6.8572831284153, 3.695461919398907], "isController": false}, {"data": ["Log into user\'s account-1", 5, 0, 0.0, 187.6, 185, 193, 186.0, 193.0, 193.0, 193.0, 23.36448598130841, 227.30176693925233, 4.312390478971962], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 64, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
