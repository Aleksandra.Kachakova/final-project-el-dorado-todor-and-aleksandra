package testCases;

import org.junit.After;
import org.junit.Test;

public class FunctionalTesting extends BaseTest{

    @After
    public void navigateToHome() {
        actions.clickElement("button.HomePage");
    }

    @Test
    public void changeFamilyName() {
        //Log in as User
        healthyFoodAPI.authenticateDriverForUser("user1.Username", "user1.Password", actions.getDriver());
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();

        //Edit family name
        profilePage.navigateToEditProfilePage();
        profilePage.updateFamilyName("new.FamilyName");
        profilePage.assertFamilyNameUpdated("new.FamilyName");

        //Change family name to initial value
        profilePage.editProfilePage();
        profilePage.updateFamilyName("default.FamilyName");
        profilePage.assertFamilyNameUpdated("default.FamilyName");

        //Log out
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();
    }

    @Test
    public void searchForPost() {
        //Log in as User
        healthyFoodAPI.authenticateDriverForUser("user1.Username", "user1.Password", actions.getDriver());
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();

        //Search for post
        postsPage.navigateToLatestPostsPage();
        postsPage.assertLatestPostsPageNavigated();
        postsPage.searchForPost("search.Word");
        postsPage.assertSearchedPostAppears("search.Word");

        //Log out
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();
    }
}
