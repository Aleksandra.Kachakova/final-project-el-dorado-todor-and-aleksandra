package testCases;
import org.junit.Test;

public class PostTests extends BaseTest {

    @Test
    public void createUpdateLikeAndDeletePost_asAdmin() {
        //Log in as Admin
        loginPage.navigateToLoginPage();
        loginPage.assertLoginPageNavigated();
        loginPage.logIn("admin.Username", "admin.Password");
        loginPage.assertUserIsLoggedIn();

        //Create new post
        postsPage.navigateToNewPostPage();
        postsPage.assertNewPostPageNavigated();
        postsPage.createPost();
        postsPage.assertPostCreated();

        //Update post
        postsPage.navigateToCreatedPostFromUserProfile();
        postsPage.updateAllPostFields();
        postsPage.assertPostFieldsUpdated();

        //Like post
        postsPage.likePost();

        //Dislike post
        postsPage.dislikePost();

        //Delete created post
        postsPage.deletePost();
        postsPage.assertPostNotPresent();

        //Log out
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();
    }
}
