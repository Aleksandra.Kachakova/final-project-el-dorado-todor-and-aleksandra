package testCases;
import org.junit.Test;

public class CommentsTests extends BaseTest{

    @Test
    public void createUpdateLikeAndDeleteComment_asUser() {
        //Log in as User
        loginPage.navigateToLoginPage();
        loginPage.assertLoginPageNavigated();
        loginPage.logIn("user2.Username", "user2.Password");
        loginPage.assertUserIsLoggedIn();

        //Create new post
        postsPage.navigateToNewPostPage();
        postsPage.assertNewPostPageNavigated();
        postsPage.createPost();
        postsPage.assertPostCreated();

        //Add comment
        postsPage.navigateToCreatedPostFromUserProfile();
        postsPage.addComment();
        postsPage.assertCommentIsAdded();

        //Like comment
        postsPage.likeComment();
        postsPage.assertCommentIsLiked();

        //Dislike comment
        postsPage.dislikeComment();
        postsPage.assertCommentIsDisliked();

        //Log out User
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();

        //Log in as Admin and navigate to comment
        loginPage.logIn("admin.Username", "admin.Password");
        loginPage.assertUserIsLoggedIn();
        postsPage.navigateToLatestPostsPage();
        postsPage.assertLatestPostsPageNavigated();
        postsPage.assertPostIsInLatestPostsPage();
        postsPage.openPostInLatestPostsPage();

        //Update comment as Admin
        postsPage.editComment();
        postsPage.updateComment();
        postsPage.assertCommentIsUpdated();

        //Delete comment as Admin
        postsPage.editComment();
        postsPage.deleteComment();

        //Delete post as Admin
        postsPage.deletePost();

        //Log out Admin
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();
    }
}
