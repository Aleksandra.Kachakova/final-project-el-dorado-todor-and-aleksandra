package testCases;
import org.junit.Test;

public class ConnectionTests extends BaseTest{

    @Test
    public void connectAndDisconnect_toAnotherUser() {
        //Log in as User One
        healthyFoodAPI.authenticateDriverForUser("user1.Username", "user1.Password", actions.getDriver());
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();

        //Send connection request to User Two
        usersPage.navigateToUsersPage();
        usersPage.assertUsersPageNavigated();
        usersPage.sendConnectionRequest();
        usersPage.assertConnectionRequestIsSent();

        //Log out User One
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();

        //Log in as User Two
        healthyFoodAPI.authenticateDriverForUser("user2.Username", "user2.Password", actions.getDriver());
        actions.clickElement("button.HomePage");
        navPage.assertPageNavigated();

        //Accept connection request
        usersPage.navigateToUserRequestsPage();
        usersPage.assertRequestIsReceived();
        usersPage.confirmConnection();
        usersPage.assertConnectionIsConfirmed();

        //Disconnect from User One
        usersPage.navigateToUserOneProfile();
        usersPage.clickOnDisconnectButton();
        usersPage.assertDisconnectedFromUser();

        //Log out User Two
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();
    }

}
