package com.telerikacademy.finalproject.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		private WebDriver driver = setupBrowser();

		//To run with alternative WebDriver - comment out current one and uncomment chosen one

			//Run with Firefox WebDriver
			private WebDriver setupBrowser(){
			WebDriverManager.firefoxdriver().setup();
			//Run in headless mode
//			FirefoxOptions firefoxOptions = new FirefoxOptions();
//			firefoxOptions.addArguments("--headless");
//			WebDriver firefoxDriver = new FirefoxDriver(firefoxOptions);
			WebDriver firefoxDriver = new FirefoxDriver();
			firefoxDriver.manage().window().maximize();
			driver = firefoxDriver;
			return firefoxDriver;
		}

			//Run with Chrome WebDriver
//			private WebDriver setupBrowser(){
//			WebDriverManager.chromedriver().setup();
//			WebDriver chromeDriver = new ChromeDriver();
//			chromeDriver.manage().window().maximize();
//			driver = chromeDriver;
//			return chromeDriver;
//		}

			//Run with Opera WebDriver
//			private WebDriver setupBrowser(){
//			WebDriverManager.operadriver().setup();
//			WebDriver operaDriver = new OperaDriver();
//			operaDriver.manage().window().maximize();
//			driver = operaDriver;
//			return operaDriver;
//		}

			//Run with Edge WebDriver
//			private WebDriver setupBrowser(){
//			WebDriverManager.edgedriver().setup();
//			WebDriver edgeDriver = new EdgeDriver();
//			edgeDriver.manage().window().maximize();
//			driver = edgeDriver;
//			return edgeDriver;
//		}

		public void quitDriver() {
			if (driver != null) {
				driver.quit();
				driver = null;
			}
		}

		public WebDriver getDriver() {
			if (driver == null){
				setupBrowser();
			}
			return driver;
		}
	}
}
