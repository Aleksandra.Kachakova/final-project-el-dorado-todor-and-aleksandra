package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;

public class ProfilePage extends BasePage{

    public ProfilePage() {
        super("profile.url");
    }

    public void navigateToEditProfilePage() {
        actions.hoverElement("navigation.ProfileSection");
        actions.clickElement("navigation.EditProfileSection");
    }

    public void updateFamilyName(String familyName) {
        actions.clearElement("profile.FamilyNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey(familyName), "profile.FamilyNameField");
        actions.clickElement("button.Submit");
    }

    public void editProfilePage() {
        actions.clickElement("button.EditProfile");
    }

    //--------------ASSERTS-----------------
    public void assertFamilyNameUpdated(String familyName) {
        Assert.assertNotNull(driver.findElement(By.xpath("//div[@class='row']//span[contains(text(), \""+Utils.getConfigPropertyByKey(familyName)+"\")]")));
        actions.waitFor(1000);
    }
}
