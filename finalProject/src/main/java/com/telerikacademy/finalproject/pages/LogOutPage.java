package com.telerikacademy.finalproject.pages;

public class LogOutPage extends BasePage{

    public LogOutPage() {
        super("logout.url");
    }

    public final String logoutButton = "button.LogOut";

    public void logOut() {
        actions.hoverElement(logoutButton);
        actions.clickElement(logoutButton);
    }
}
