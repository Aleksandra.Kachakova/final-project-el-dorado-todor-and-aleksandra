package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {

    public NavigationPage() {
        super("base.url");
    }
    public final String homeButton = "navigation.Home";
}